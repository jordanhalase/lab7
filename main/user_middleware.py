from main.models import User


class UserMiddleware:
    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        #Dont do any of our user session handling when on admin site
        if request.path.startswith("/admin/"):
            return self.get_response(request)

        # Before view and other middleware
        request.user = None
        request.auth = False
        customCtx = {"auth": False}
        if hasattr(request, "customCtx"):
            customCtx = request.customCtx

        username = request.session.get("user", None)
        if username:
            users = list(User.objects.filter(name=username))
            if len(users) > 0:
                user = users[0]
                request.user = user
                request.auth = True
                customCtx = {"auth": True, "user": user, "username": user.name}

        request.customCtx = customCtx
        response = self.get_response(request)
        # "After" view
        return response
