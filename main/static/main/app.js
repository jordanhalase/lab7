axios.defaults.xsrfCookieName = 'csrftoken'
axios.defaults.xsrfHeaderName = "X-CSRFTOKEN"

function cmd(e) {
    e.preventDefault();

    let cmdElement = document.getElementById("command");

    let data = {
        "command": cmdElement.value
    };

    cmdElement.value = "";

    axios.post('/api/cmd', JSON.stringify(data))
        .then(function (res) {
            newResponse(res.data.response);
        })
        .catch(function (err) {
            newResponse(err.value, true)
        });

    return false;
}

function newResponse(msg, error=false) {
    let cmdBox = document.getElementById('cmdList');
    let li = document.createElement("li");
    li.innerText = msg
    cmdBox.prepend(li);
}
