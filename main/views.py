import json

from django.http import HttpResponse, JsonResponse
from django.shortcuts import render, redirect
from django.views import View

from main.messages import err_userNotFound

from main.models import User, Table
from main.cmd.factory import MakeCaller


class Home(View):
    def get(self, request):
        return render(request, 'main/index.html', context=request.customCtx)

    def post(self, request):
        username = request.POST["username"]
        command = request.POST["command"]

        users = list(User.objects.filter(name=username))
        if len(users) == 0:
            username = ""
            response = err_userNotFound
        else:
            user = users[0]
            response = MakeCaller(user).execute(command)

        request.customCtx["response"] = response
        return render(request, 'main/index.html', context=request.customCtx)


class Cmd(View):
    def get(self, request):
        return redirect('/')

    def post(self, request):
        if not request.auth:
            return redirect('/')

        body_unicode = request.body.decode('utf-8')
        data = json.loads(body_unicode)
        command = data["command"]

        payload = {}
        payload = {
            "response": MakeCaller(request.user).execute(command)
        }

        return JsonResponse(data=payload)


class Logout(View):
    def get(self, request):
        if request.auth:
            del request.session["user"]

        return redirect('/login')


class Login(View):
    def get(self, request):
        if request.auth:
            return redirect('/')

        return render(request, "main/login.html", context=request.customCtx)

    def post(self, request):
        if request.auth:
            return redirect('/')

        username = request.POST.get("username", "")
        password = request.POST.get("password", "")
        users = list(User.objects.filter(name=username))

        error = ""
        if len(users) > 0:
            user = users[0]
            if user.password == password:
                request.session["user"] = username
                return redirect("/")
            else:
                error = "Username and password do not match!"
        else:
            error = "User not found!"

        request.customCtx["error"] = error
        return render(request, "main/login.html", context=request.customCtx)


class Register(View):
    def get(self, request):
        if request.auth:
            del request.session["user"]
            return redirect('register')

        return render(request, "main/register.html", context=request.customCtx)

    def post(self, request):
        username = request.POST.get("username", "")
        password = request.POST.get("password", "")
        password_confirm = request.POST.get("password_confirm", "")

        users = list(User.objects.filter(name=username))

        error = ""
        if len(users) == 0:
            if 0 < len(username) <= 100:
                if 0 < len(password) <= 100:
                    if password == password_confirm:
                        u = User(name=username, password=password)
                        u.save()
                        request.session["user"] = username
                        return redirect('/')
                    else:
                        error = "Passwords do not match"
                else:
                    error = "Invalid password"
            else:
                error = "Invalid username"
        else:
            error = "User already exists"

        request.customCtx["error"] = error
        return render(request, "main/register.html", context=request.customCtx)


class TableView(View):
    def get(self, request, table_name=None, action=None):
        if not request.auth:
            return redirect('/login')

        if not table_name:
            if request.user.atTable():
                table_name = request.user.table.getURLSlug()
            else:
                return redirect('/tables')

        tables = Table.objects.filter(slug=table_name)
        if len(tables) == 0:
            return redirect('/tables')

        table = tables[0]
        request.customCtx["table"] = table
        if action == "join":
            table.addUser(request.user)
            return redirect('/table', context=request.customCtx)

        if action == "call":
            game = table.game
            game.playerAction(request.user, game.Action.Call)
            return redirect('/table', context=request.customCtx)

        if action == "fold":
            game = table.game
            msg = game.playerAction(request.user, game.Action.Fold)
            request.customCtx["response"] = msg
            return redirect('/table', context=request.customCtx)


        return render(request, 'main/table.html', context=request.customCtx)

    def post(self, request, table_name=None, action=None):
        tables = Table.objects.filter(slug=table_name)
        if len(tables) == 0:
            return redirect('/tables')
        table = tables[0]

        if action == "betraise":
            game = table.game
            amt = request.POST.get("amt", "")
            amt = int(amt)
            msg = ""
            if request.POST.get("bet"):
                msg = game.playerAction(request.user, game.Action.Bet, amt)
            elif request.POST.get("raise"):
                msg = game.playerAction(request.user, game.Action.Raise, amt)
            request.customCtx["response"] = msg

        return redirect(f'/table/{table_name}', context=request.customCtx)


class Tables(View):
    def get(self, request):
        if not request.auth:
            return redirect('/login')

        request.customCtx["tables"] = Table.objects.all()
        return render(request, 'main/tables.html', context=request.customCtx)

    def post(self, request, action):
        table = request.POST.get("table", "")
        if action == "add":
            tables = list(Table.objects.filter(name=table))

            error = ""
            if len(tables) == 0:
                if 0 < len(table) <= 100:
                    T = Table(name=table)
                    T.save()
                    request.customCtx["response"] = "Table added successfully"

                else:
                    error = "Invalid table name"
            else:
                error = "Table name already exists"
            request.customCtx["error"] = error
            return redirect('/tables', context=request.customCtx)
        elif action == "delete":
            tables = list(Table.objects.filter(name=table))
            msg = ""
            if len(tables) > 0:
                tables[0].delete()
                msg = f"Deleted table: {table}"
            else:
                msg = "Table not found"

            request.customCtx["response"] = msg
            return redirect('/tables', context=request.customCtx)


class Users(View):
    def get(self, request):
        if not request.auth:
            return redirect('/')

        if not request.user.admin:
            return redirect('/')
        request.customCtx["users"] = User.objects.all()
        return render(request, 'main/users.html', context=request.customCtx)

    def post(self, request, action):

        username = request.POST.get("username", "")
        if action == "add":
            users = list(User.objects.filter(name=username))
            password = request.POST.get("password", "")
            password_confirm = request.POST.get("password_confirm", "")
            error = ""
            if len(users) == 0:
                if 0 < len(username) <= 100:
                    if 0 < len(password) <= 100:
                        if password == password_confirm:
                            u = User(name=username, password=password)
                            u.save()
                            request.customCtx["response"] = "User added successfully"
                        else:
                            error = "Passwords do not match"
                    else:
                        error = "Invalid password"
                else:
                    error = "Invalid username"
            else:
                error = "User already exists"
            request.customCtx["error"] = error
            return redirect('/users', context=request.customCtx)
        elif action == "delete":
            users = list(User.objects.filter(name=username))
            msg = ""
            if len(users) > 0:
                users[0].delete()
                msg = f"Deleted user: {username}"
            else:
                msg = "User not found"

            request.customCtx["response"] = msg
            return redirect('/users', context=request.customCtx)
