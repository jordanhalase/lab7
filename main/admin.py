from django.contrib import admin

# Register your models here.
from main import models

admin.site.register(models.User)
admin.site.register(models.Table)
admin.site.register(models.Game)
