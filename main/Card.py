class Card:
    '''
    SUITS
    1: Clubs
    2: Spades
    3: Heart
    4: Diamond
    '''

    def __init__(self, suit, rank):
        # initialize a card to the given suit (1-4) and rank (1-13)
        # 1:clubs 2:spades 3:hearts 4:diamonds
        # raise an exception for invalid argument, ValueError
        if type(suit) is not int or type(rank) is not int:
            raise ValueError

        if suit not in range(1, 5):
            raise ValueError

        if rank not in range(1, 14):
            raise ValueError
        self.suit = suit
        self.rank = rank

    def __eq__(self, other):
        return self.suit == other.suit and self.rank == other.rank

    @staticmethod
    def fromString(value):
        if type(value) is not str or len(value) > 3:
            raise ValueError
        s = value[0]
        r = value[1]
        if len(value) == 3:
            r += value[2]

        try:
            s = int(s)
            r = int(r)
        except:
            raise ValueError

        return Card(s, r)

    def toString(self):
        return str(self.getSuit()) + str(self.getRank())

    def getSuit(self):
        # return the suit of the card (1-4)
        return self.suit

    def getRank(self):
        # return the rank of the card (1-13)
        return self.rank

    def getValue(self):
        # game is suit * rank
        # get the game-specific value of a Card
        # if not used, or game is not defined, always returns 0.
        return self.suit * self.rank

    def __str__(self):
        # return rank and suite, as AS for ace of spades, or 3H for
        # three of hearts, JC for jack of clubs.
        strRep = ""

        if self.rank == 13:
            strRep += "K"
        elif self.rank == 12:
            strRep += "Q"
        elif self.rank == 11:
            strRep += "J"
        elif self.rank == 1:
            strRep += "A"
        else:
            strRep += str(self.rank)

        if self.suit == 1:
            strRep += "C"
        elif self.suit == 2:
            strRep += "S"
        elif self.suit == 3:
            strRep += "H"
        elif self.suit == 4:
            strRep += "D"

        return strRep
