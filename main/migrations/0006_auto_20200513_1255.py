# Generated by Django 3.0.5 on 2020-05-13 17:55

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0005_auto_20200503_1507'),
    ]

    operations = [
        migrations.AddField(
            model_name='user',
            name='password',
            field=models.CharField(default='password', max_length=100, null=True),
        ),
        migrations.AlterField(
            model_name='table',
            name='game',
            field=models.OneToOneField(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='main.Game'),
        ),
    ]
