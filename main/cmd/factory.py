from main.cmd.admin import AdminCaller
from main.cmd.user import UserCaller

def MakeCaller(caller):
    if caller.admin:
        return AdminCaller(caller)
    else:
        return UserCaller(caller)
