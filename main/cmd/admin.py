from main.apps import MainConfig
from main.cmd.caller import Caller
from main.cmd.user import UserCaller
from main.messages import err_cmd_empty, err_cmd_needSubCmd, err_limit_tables, err_cmd_createAdmin_nameValidate
from main.models import User, Table


class AdminCaller(Caller):
    def execute(self, cmdStr):
        args = cmdStr.split(" ")
        if len(args) == 0:
            return err_cmd_empty

        cmd = args[0]
        sub_args = args[1::]
        if cmd == "create":
            return self.sub_create(sub_args)

        return UserCaller(self.caller).execute(cmdStr)

    def sub_create(self, args):
        if len(args) < 1:
            return err_cmd_needSubCmd
        cmd = args[0]
        sub_args = args[1::]

        if cmd == "admin":
            if len(sub_args) < 1 or not str(sub_args[0]).isupper():
                return err_cmd_createAdmin_nameValidate
            adminUsername = str(sub_args[0]).upper()
            return self.createUser(adminUsername, admin=True)
        elif cmd == "user":
            if len(sub_args) < 1:
                return "Please enter a username"
            return self.createUser(sub_args[0])
        elif cmd == "table":
            name = ""
            if len(sub_args) >= 1:
               name = " ".join(sub_args)
            return self.createTable(name)


    def createUser(self, username, admin=False):
        obj, created = User.objects.get_or_create(name=username, admin=admin)
        if created:
            if admin:
                return f'Admin "{obj.name}" created successfully'
            return f'User "{obj.name}" created successfully'
        else:
            return "could not create user"


    def createTable(self, name):
        numTables = Table.objects.count()
        maxTables = MainConfig.maxTables
        if numTables >= maxTables:
            return err_limit_tables

        ##Validate name
        if name == "":
            name = f'table{numTables + 1}'

        obj, created = Table.objects.get_or_create(name=name)
        if created:
            return "Successfully created table: " + name
        else:
            return "Table not created"
