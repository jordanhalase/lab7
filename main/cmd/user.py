from main.cmd.caller import Caller
from main.messages import err_cmd_notFound, err_cmd_needSubCmd, err_cmd_atTable, err_cmd_notAtTable, \
    err_cmd_empty, err_cmd_tableNotFound, err_cmd_subArgInt, err_cmd_notAtTableRefresh
from main.models import Table, Game, User


class UserCaller(Caller):
    def execute(self, cmdStr):
        args = cmdStr.split(" ")
        if len(args) == 0:
            return err_cmd_empty

        cmd = args[0]
        sub_args = args[1::]
        atTable = self.caller.atTable()

        if cmd == "refresh":
            if atTable:
                if self.caller.inGame():
                    game = self.caller.game
                    hand = self.caller.getHand()
                    return f'Pot: {game.pot}\n' \
                           f'Current Bet: {game.highestBet}\n' \
                           f'Turn: {game.current_player.name}\n' \
                           f'Chips: {self.caller.chips}\n' \
                           f'Cards ({hand.value()}): {hand.printStr()}'
                else:
                    table = self.caller.table
                    pString = ', '.join(map(User.__str__, table.getPlayers()))
                    return f"Table: {table.name}\n" \
                           f"Players: {pString}"

            return err_cmd_notAtTableRefresh

        if cmd == "join":
            if atTable:
                return err_cmd_atTable
            return self.sub_join(sub_args)
        elif cmd == "list":
            if atTable:
                return err_cmd_atTable
            return self.sub_list(sub_args)

        elif cmd == "play":  # when sitting at table and want to play game
            if atTable:
                table = self.caller.table
                if self.caller.inGame():
                    return "You are already in a game"
                if table.getPlayerCount() < 2:
                    return "You need at least two players before starting game"
                table.startGame()
                return "Game started"

            return err_cmd_notAtTable
        elif cmd == "leave":
            if atTable:
                if self.caller.inGame():
                    return "Error: cannot leave while in a game. Use the fold command on your turn."
                self.caller.table.removeUser(self.caller)
                return "Left the table!"
            return err_cmd_notAtTable

        if self.caller.inGame():
            amt = 0
            if len(sub_args) > 0:
                try:
                    amt = int(sub_args[0])
                except ValueError:
                    return err_cmd_subArgInt

            game = self.caller.game
            if cmd == "chips":
                str = "Player Chips:\n"
                for p in game.getPlayers():
                    str += f'{p.name}: {p.chips}\n'
                return str
            elif cmd == "cards":
                return game.playerAction(self.caller, Game.Action.Cards)
            elif cmd == "raise":  # [Amt]
                return game.playerAction(self.caller, Game.Action.Raise, amt)
            elif cmd == "call":
                return game.playerAction(self.caller, Game.Action.Call)
            elif cmd == "check":
                return game.playerAction(self.caller, Game.Action.Check)
            elif cmd == "bet":  # [Amt]
                return game.playerAction(self.caller, Game.Action.Bet, amt)
            elif cmd == "fold":
                return game.playerAction(self.caller, Game.Action.Fold)
            else:
                return err_cmd_notFound
        else:
            return err_cmd_notFound

    def sub_join(self, args):
        if len(args) == 0:
            return "please enter a table name"

        tableName = " ".join(args)
        tables = list(Table.objects.filter(name=tableName))
        if len(tables) == 0:
            return err_cmd_tableNotFound

        table = tables[0]
        table.addUser(self.caller)

        return f'Joined {table.name}'

    def sub_list(self, args):
        if len(args) < 1:
            return err_cmd_needSubCmd
        cmd = args[0]
        sub_args = args[1::]

        if cmd == "tables":
            if len(sub_args) != 0:
                return "No arguments allowed"
            tables = list(Table.objects.all())
            return ", ".join(map(str, tables))
        else:
            return err_cmd_notFound
