import random

from django.db import models

from main.Card import Card


# Length of a full deck in string form with comma separators
def MAX_LEN(): return 171


class Deck:
    def __init__(self, deck=None):
        if deck is not None:
            self.deck = deck
            self.sort()
            return

        # create a standard 52 card deck of cards
        self.deck = []
        for suit in range(1, 5):
            for rank in range(1, 14):
                self.deck.append(Card(suit, rank))

        self.deck.reverse()

    @staticmethod
    def fromString(value):
        if type(value) is not str or len(value) > MAX_LEN():
            raise ValueError
        if len(value) == 0:
            return Deck(deck=[])

        cardStrings = value.split(',')
        cards = list(map(Card.fromString, cardStrings))
        return Deck(deck=cards)

    def toString(self):
        strList = map(Card.toString, self.deck)
        return ','.join(strList)

    def printStr(self):
        return ', '.join(map(Card.__str__, self.deck))

    def count(self):
        # return the number of cards remaining in the deck
        return len(self.deck)

    def value(self):
        value = 0

        card1 = self.deck[0]
        highCard = card1.getRank()
        for i in range(1, 4):
            card2 = self.deck[i]
            if card2.getRank() > card1.getRank():
                highCard = card2.getRank()
        value = highCard

        val = self.check_pair()
        if val > 0:
            value = 54 + 13 + val

        val = self.check_two_pair()
        if val > 0:
            value = 54 + 13*2 + val

        val = self.check_triple()
        if val > 0:
            value = 54 + 13*3 + val

        val = self.check_straight()
        if val > 0:
            value = 54 + 13*4 + val

        val = self.check_flush()
        if val > 0:
            value = 54 + 13*5 + val

        val = self.check_full_house()
        if val > 0:
            value = 54 + 13*6 + val

        val = self.check_quad()
        if val > 0:
            value = 54 + 13*7 + val

        val = self.check_straight_flush()
        if val > 0:
            value = 54 + 13*8 + val

        val = self.check_royal_flush()
        if val > 0:
            value = 54 + 13*9 + val

        return value

    def check_royal_flush(self):
        val1 = self.check_straight_flush()
        if val1 > 0:
            for card in self.deck:
                if card.getRank() == 1:
                    return 100
        return -1

    def check_straight_flush(self):
        val1 = self.check_flush()
        val2 = self.check_straight()
        if val1 > 0 and val2 > 0:
            return val2
        else:
            return -1

    def check_quad(self):
        alike = 0
        for card1 in self.deck:
            alike = 0
            for card2 in self.deck:
                if card1.getRank() == card2.getRank():
                    alike += 1
                if alike == 4:
                    return card1.getRank()
        return -1

    def check_full_house(self):
        if self.check_triple() == -1 or self.check_pair() == -1 or self.check_quad() > 0:
            return False
        # for i in range(0, 4):

        card1 = self.deck[0]
        alike1 = 0
        alike2 = 0
        for j in range(0, 4):
            card2 = self.deck[j]
            if card1.getRank() == card2.getRank():
                alike1 += 1
            elif (alike1 == 3 and alike2 == 2) or (alike1 == 2 and alike2 == 3):
                if card1.getRank() > card2.getRank():
                    return card1.getRank()
                else:
                    return card2.getRank()
            else:
                for k in range(j, 4):
                    card3 = self.deck[k]
                    if card2.getRank() == card3.getRank():
                        alike2 += 1
        return -1

    def check_flush(self):
        flush = 0
        highCard = 0
        card1 = self.deck[0]
        highCard = card1.getRank()
        for i in range(1, 4):
            card2 = self.deck[i]
            if highCard < card2.getRank():
                highCard = card2.getRank()
            if card1.getSuit() == card2.getSuit():
                flush += 1
        if flush == 4:
            return highCard
        return -1

    def check_straight(self):
        straight = 0
        card1 = self.deck[0]
        highCard = card1.getRank()
        for i in range(1, 4):
            card2 = self.deck[i]
            highCard = card2.getRank()
            if card1.getRank() == card2.getRank() + i:
                straight += 1
        if straight == 4:
            return highCard
        return -1

    def check_triple(self):
        for card1 in self.deck:
            alike = 0
            for i in range(0, 4):
                card2 = self.deck[i]
                if card1.getRank() == card2.getRank():
                    alike += 1
                if alike == 3:
                    return card2.getRank()
        return -1

    def check_two_pair(self):
        alike1 = 0
        alike2 = 0

        card1 = self.deck[0]
        highCard = card1.getRank()
        for i in range(0, 4):
            card2 = self.deck[i]
            if card1.getRank() == card2.getRank():
                alike1 += 1
            elif alike1 >= 2 and alike2 >= 2:
                return highCard
            else:
                alike2 = 0
                for j in range(i, 4):
                    card3 = self.deck[j]
                    if card2.getRank() == card3.getRank():
                        alike2 += 1
                        if highCard < card2.getRank():
                            highCard = card2.getRank()
        return -1

    def check_pair(self):
        for card1 in self.deck:
            for i in range(1, 4):
                card2 = self.deck[i]
                if card1.getRank() == card2.getRank():
                    return card1.getRank()
        return -1

    def draw(self):
        # return and remove the top card in the deck
        # if the deck is empty, raise a ValueError
        if len(self.deck) < 1:
            raise ValueError
        return self.deck.pop()

    def shuffle(self):
        # shuffle the deck using a random number generator
        random.shuffle(self.deck)
        return self

    def sort(self):
        list.sort(self.deck, key=Card.getSuit)
        list.sort(self.deck, key=Card.getRank)

    def clear(self):
        self.deck = []


class DeckField(models.Field):
    def db_type(self, connection):
        return f'VARCHAR({MAX_LEN()})'

    def __init__(self, *args, deck=None, **kwargs):
        kwargs["max_length"] = MAX_LEN()
        kwargs["default"] = Deck(deck=deck)
        super().__init__(*args, **kwargs)

    def deconstruct(self):
        name, path, args, kwargs = super().deconstruct()
        del kwargs["max_length"]
        del kwargs["default"]
        return name, path, args, kwargs

    # DB to python obj
    def from_db_value(self, value, expression, connection):
        if value is None:
            return None
        return Deck.fromString(value)

    # None, String, or Obj to python obj
    def to_python(self, value):
        if isinstance(value, Deck):
            return value
        if value is None:
            return value

        return Deck.fromString(value)

    def get_prep_value(self, value):
        if not value:
            return value
        if isinstance(value, str):
            return value

        return value.toString()

    # # Python Obj to string
    def value_to_string(self, obj):
        value = self.value_from_object(obj)
        return value.toString()

    #
    # def get_db_prep_value(self, value, connection, prepared=False):
    #     if value is None:
    #         return value
    #     if isinstance(value, Deck):
    #         return value.toString()
    #     return value
    #
