from django.apps import AppConfig


class MainConfig(AppConfig):
    name = 'main'

    maxTables = 10

    limit_bet = 5

    default_numSeats = 5
    default_pot = 0
    default_chips = 100
