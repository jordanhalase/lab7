
def error(msg):
    return "Error: " + msg


err_userNotFound = error("user not found")

err_cmd_createAdmin_nameValidate = error("Please enter a username in all uppercase")
err_cmd_createUserFailed = error("could not create user")
err_cmd_empty = error("please enter a command")
err_cmd_notFound = error("command not found")
err_cmd_needSubCmd = error("please enter a subcommand")
err_cmd_atTable = error("cannot use this command while sitting at a table")
err_cmd_notAtTable = error("you must be sitting at a table to use this command")
err_cmd_notInGame = error("you must be in a game to use this command")
err_cmd_subArgInt = error("argument must be an integer")
err_cmd_notAtTableRefresh = error("Not at a table. Use list tables to see which ones you can join")
err_cmd_tableNotFound = error("There is not a table with this name")
err_cmd_notAdmin = error("You must be an admin to enter this command")
err_limit_tables = error("already at max number of tables")

