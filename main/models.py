from enum import Enum

from django.db import models
from django.db.models.deletion import SET_NULL
from django.utils.text import slugify

from main.Deck import DeckField
from main.apps import MainConfig


class Game(models.Model):
    class Action(Enum):
        Cards = 1
        Raise = 2  # [Amt]
        Call = 3
        Check = 4
        Bet = 5  # [Amt]
        Fold = 6

    pot = models.IntegerField(default=MainConfig.default_pot)

    highestBet = models.IntegerField(default=0)
    turnIndex = models.IntegerField(default=0)
    current_player = models.ForeignKey('User', null=True, on_delete=SET_NULL, related_name="_unused_1")
    deck = DeckField()

    def canStart(self):
        return self.table.getPlayerCount() > 1

    def start(self):
        if not self.canStart():
            raise Exception("game requirements not met")

        self.deck = self.deck.shuffle()

        players = self.table.getPlayers()
        for user in players:
            user.onJoinGame(self)

        players = self.getPlayers()
        self.current_player = players[0]
        self.turnIndex = players.index(self.current_player)
        self.deal()
        self.save()

    def deal(self):
        for p in self.getPlayers():
            p.draw(self.deck)
        self.save()

    def playerAction(self, player, a: Action, amt: int = 0):
        # Actions that can be taken anytime
        if a is Game.Action.Cards:
            return f'Your cards: {player.getHand().printStr()}'

        # Actions that can be taken only on your turn and which advance the game
        msg = ""
        completedTurn = False

        if self.current_player != player:
            msg = "You must wait your turn"
        else:
            if a is Game.Action.Raise:  # [Amt]
                if self.highestBet == 0:
                    msg = f'There is no current bet. Use bet instead.'
                elif amt < 1 or amt > MainConfig.limit_bet:
                    msg = f'Invalid raise amount, please raise between 1 and {MainConfig.limit_bet} chips'
                elif amt + self.highestBet > MainConfig.limit_bet:
                    msg = f'Bet limit reached! Call or check.'
                else:
                    if player.subChips(amt):
                        self.highestBet += amt
                        self.save()
                        msg = f"Raised by {amt} chips"
                        completedTurn = True
                    else:
                        msg = f"You cannot afford to raise by {amt} chips"
            elif a is Game.Action.Call:
                if self.highestBet == 0:
                    msg = "You cannot call. You must bet or fold."
                elif player.subChips(self.highestBet):
                    self.pot += amt
                    self.save()
                    msg = "Called!"
                    completedTurn = True
            elif a is Game.Action.Check:
                if self.highestBet == 0:
                    msg = f'Checked!'
                    completedTurn = True
                else:
                    msg = f'You cannot check because someone has bet already'
            elif a is Game.Action.Bet:  # [Amt]
                if player.currentBet < self.highestBet:
                    msg = f"You must raise."
                elif amt < 1 or amt > MainConfig.limit_bet:
                    msg = f'Invalid bet amount, please bet between 1 and {MainConfig.limit_bet} chips'
                else:
                    if player.subChips(amt):
                        player.currentBet = amt
                        player.save()
                        self.pot += amt
                        self.save()

                        self.highestBet = amt
                        self.save()
                        completedTurn = True
                        msg = f'Placed bet of {amt} chips'
                    else:
                        msg = f'You only have {player.chips} chips'
            elif a is Game.Action.Fold:
                self.foldPlayer(player)
                completedTurn = True
                msg = "Folded. Better luck next time!"

        if completedTurn:
            gameMsg = self.nextPlayer()
            if len(gameMsg) > 0:
                msg += f'\n{gameMsg}'

        return msg

    def nextPlayer(self):
        gameMsg = ''
        players = self.getPlayers()
        self.turnIndex += 1
        # find a winner - who's ever chip count is lower is winner that means that bet the most
        # fold everyone

        # Everyone has bet
        if self.turnIndex >= len(players):
            self.turnIndex = 0

            winner = players[0]
            # loop through players
            # find chips
            for player in players:
                if player.getHand().value() > winner.getHand().value():
                    winner = player

            gameMsg = f'{winner.name} won and received {self.pot} chips!\n' \
                      f'Ending game!'
            # set highests to chips plus pot
            winner.chips = self.pot + winner.chips
            winner.save()
            self.pot = 0
            # loop through all players and fold
            for player in players:
                self.foldPlayer(player)
            self.table.endGame()

        self.current_player = players[self.turnIndex]
        self.save()

        return gameMsg

    def getPlayers(self):
        return list(User.objects.filter(game=self))

    def foldPlayer(self, user):
        if user.game != self:
            raise Exception
        user.onLeaveGame()


class Table(models.Model):
    name = models.CharField(max_length=100)
    slug = models.SlugField(max_length=100, blank=True, default="auto")
    seats = models.IntegerField(default=MainConfig.default_numSeats)
    game = models.OneToOneField(Game, blank=True, null=True, on_delete=SET_NULL)

    def save(self, *args, **kwargs):
        self.slug = slugify(self.name)
        super(Table, self).save(*args, **kwargs)

    def __str__(self):
        return f'{self.name}[{self.getPlayerCount()}/{self.seats}]'

    def getURLSlug(self):
        return self.slug

    def getPlayerCount(self):
        return User.objects.filter(table=self).count()

    def getPlayers(self):
        return list(User.objects.filter(table=self))

    def getSeats(self):
        players = self.getPlayers()
        numSeats = MainConfig.default_numSeats
        if len(players) < numSeats:
            players = players + [None] * (numSeats - len(players) - 1)
        return players

    def addUser(self, user):
        if self.getPlayerCount() >= self.seats:
            raise Exception
        user.onJoinTable(self)
        if not self.game and self.getPlayerCount() > 1:
            self.startGame()

    def removeUser(self, user):
        user.onLeaveTable()

    def startGame(self):
        if self.game is not None:
            raise Exception("Table.game should be null before calling the method: startGame")
        self.game = Game.objects.create()
        self.save()
        self.game.start()

    def endGame(self):
        if self.game is None:
            raise Exception("Table.game should not be null when calling the method: endGame")
        self.game = None
        self.save()


class User(models.Model):
    admin = models.BooleanField(default=False)
    name = models.CharField(max_length=100)
    password = models.CharField(max_length=100, default="password", null=True)
    highScore = models.IntegerField(default=0)
    chips = models.IntegerField(default=100)
    currentBet = models.IntegerField(default=0)

    table = models.ForeignKey(Table, blank=True, null=True, on_delete=SET_NULL)

    game = models.ForeignKey(Game, blank=True, null=True, on_delete=SET_NULL)
    game_queue = models.ForeignKey(Game, blank=True, null=True, on_delete=SET_NULL, related_name="_unused_1")
    hand = DeckField(deck=[], null=True, blank=True)

    def __str__(self):
        return self.name

    # Returns true if user is at table, false otherwise
    def atTable(self):
        return self.table is not None

    # Returns true if user is in a game, false otherwise
    def inGame(self):
        return self.game is not None

    # Returns true if user is waiting to join a game in-progress, false otherwise
    def inQueue(self):
        return self.game_queue is not None

    # Updates the highscore of a user if their current chips are greater than it
    def updateHighscore(self):
        if self.chips > self.highScore:
            self.highScore = self.chips
        self.save()

    # Resets the user's chips to 0
    def resetChips(self):
        self.chips = 0
        self.save()

    def draw(self, deck, num=5):
        for i in range(num):
            self.hand.deck.append(deck.draw())
        self.hand.sort()
        self.save()

    def getHand(self):
        return self.hand

    # Subtracts <amt> chips from the player,
    # returns true if successful, false otherwise
    # Precondition: amt is a positive integer
    def subChips(self, amt):
        if self.chips < amt:
            return False
        self.chips -= amt
        self.save()
        return True

    # Adds <amt> chips to the player,
    # Precondition: amt is a positive integer
    def addChips(self, amt):
        self.chips += amt
        self.save()

    # Gets called when the user joins a table
    def onJoinTable(self, table):
        self.table = table
        self.save()

    # Gets called when a user leaves their table
    def onLeaveTable(self):
        if not self.atTable():
            raise Exception("invalid state, onLeaveTable should only be called when at a table")
        if self.inGame():
            self.onLeaveGame()
            raise Exception("Game field should be reset elsewhere, invalid state")
        self.table = None
        self.save()

    # Gets called when a user joins a game
    # User should receive the default amount of chips
    def onJoinGame(self, game):
        self.game = game
        self.chips = MainConfig.default_chips
        self.save()

    # Gets called when a user folds from a game
    # User should have their highscore updated and their chips reset
    def onLeaveGame(self):
        self.updateHighscore()
        self.resetChips()
        self.currentBet = 0
        self.hand.clear()
        self.game = None
        self.save()
