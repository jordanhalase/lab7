"""
WSGI config for pokerApp project.

It exposes the WSGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/3.0/howto/deployment/wsgi/
"""

import os
from pokerApp import settings as defaults
from django.core.wsgi import get_wsgi_application

os.environ.setdefault('DJANGO_SETTINGS_MODULE', '.'.join([defaults.PROJECT_NAME, 'settings']))

application = get_wsgi_application()
