"""pokerApp URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.conf.urls import url
from django.contrib import admin
from django.urls import path, re_path

from main import views
from main.views import Register, TableView, Tables, Users, Login, Home, Logout

urlpatterns = [
    re_path(r'^admin/', admin.site.urls),
    path('api/cmd', views.Cmd.as_view()),
    path('table/', TableView.as_view()),
    path('table/<slug:table_name>', TableView.as_view()),
    path('table/<slug:table_name>/<action>', TableView.as_view()),
    path('register/', Register.as_view()),
    path('tables/', Tables.as_view()),
    path('tables/<action>', Tables.as_view()),
    path('users/', Users.as_view()),
    path('users/<action>', Users.as_view()),
    path('login/', Login.as_view()),
    path('logout/', Logout.as_view()),
    path('', Home.as_view()),
]
