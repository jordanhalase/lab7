from django.test import TestCase
from main import models
from main.apps import MainConfig

from main.cmd.factory import MakeCaller


# PBI - User: I want to be able to join a table and have a game start if there's at least one other player
class TestJoinNewGame(TestCase):
    def setUp(self):
        self.table = models.Table.objects.create(name="table")
        self.user1 = models.User.objects.create(admin=False, name='user1')
        self.table.addUser(self.user1)

        self.user2 = models.User.objects.create(admin=False, name='user2')

    # testing to make sure player is in game
    def test_joinGame(self):
        self.assertIsNone(self.table.game, "there should be no game running")
        self.assertTrue(self.table.getPlayerCount() == 1, "there should be one player before joining")

        response = MakeCaller(self.user2).execute(f'join {self.table.name}')

        self.table.refresh_from_db()
        self.user1.refresh_from_db()
        self.user2.refresh_from_db()

        self.assertIsNotNone(self.table.game, "there should be a new game running")
        self.assertTrue(self.user1.inGame(), "user1 should be in game")
        self.assertTrue(self.user2.inGame(), "user2 should be in game")

