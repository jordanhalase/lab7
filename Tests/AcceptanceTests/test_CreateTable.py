from django.test import TestCase
from main import models
from main.apps import MainConfig
from main.cmd.factory import MakeCaller


# PBI - Admin can create tables
from main.models import Table


class TestCreateTable(TestCase):
    def setUp(self):
        self.table1 = models.Table.objects.create(name="table1")
        self.table2 = models.Table.objects.create(name="table2")
        self.admin = models.User.objects.create(admin=True, name='ADMIN')
        self.user = models.User.objects.create(admin=False, name='user')


    # Admin enters a command "create_table" to create a new table with the default number of seats
    def test_createTable(self):
        tableName = "table"
        resp = MakeCaller(self.admin).execute(f'create table {tableName}')

        self.assertEqual(resp, f'Successfully created table: {tableName}', "wrong response")
        tables = list(models.Table.objects.filter(name=tableName))
        self.assertTrue(len(tables) > 0, "should have a table")
        table = tables[0]
        self.assertEqual(table.name, tableName, "table name somehow incorrect")
        self.assertEqual(table.seats, MainConfig.default_numSeats, "wrong number of seats")

    # If the command is entered by a non-admin the system will throw an error
    def test_name(self):
        table = 'table'
        self.assertRaises(Exception, MakeCaller(self.user).execute("create table" + table))

    # If the ADMIN creates a table when 4 are currently active system will print string "Table capacity is at full"
    def test_tableError(self):
        table = 'table'
        i = Table.objects.count()
        while i < MainConfig.maxTables:
            Table.objects.create(name=table + str(i))
            i+=1

        self.assertRaises(Exception, MakeCaller(self.user).execute("create table" + table))

    #Tables can not have the same name
    def test_tableName(self):
        cmd = f"create table table"
        MakeCaller(self.admin).execute(cmd)
        self.assertRaises(Exception, MakeCaller(self.admin).execute(cmd))
