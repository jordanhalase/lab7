from django.test import TestCase
from main import models

from main.cmd.factory import MakeCaller

# PBI - I want to be able to list the available tables so I can join one
class TestListTables(TestCase):
    def setUp(self):
        self.admin = models.User.objects.create(admin=True, name='ADMIN')
        self.user = models.User.objects.create(admin=False, name='user')
        self.userCaller = MakeCaller(self.user)
        self.tableName = 'table'
        MakeCaller(self.admin).execute(f'create table {self.tableName}')

    # If I send the command "list tables", a list of named tables is returned
    def test_listTables(self):
        response = self.userCaller.execute("list tables")
        self.assertTrue(response.count(self.tableName) != 0, "there should be at least the one table")

