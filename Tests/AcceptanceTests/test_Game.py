from django.test import TestCase
from main import models
from main.apps import MainConfig

from main.cmd.factory import MakeCaller


# PBI - User: I want to be able to play a game
class TestGame(TestCase):
    def setUp(self):
        self.table = models.Table.objects.create(name="table")
        self.user1 = models.User.objects.create(admin=False, name='user1')
        self.user2 = models.User.objects.create(admin=False, name='user2')

    def test_playGame(self):
        self.table.addUser(self.user1)
        self.table.addUser(self.user2)

        u1 = MakeCaller(self.user1)
        u2 = MakeCaller(self.user2)

        self.refreshHandles()
        game = self.table.game

        self.assertIsNotNone(game, "a game should be started")
        self.assertIsNotNone(game.current_player)

        winner = self.user1
        if self.user2.getHand().value() > self.user1.getHand().value():
            winner = self.user2

        # Betting round
        u1.execute("bet 5")
        u2.execute("call")


        #Betting round over
        # Decide who won and give them the pot
        # for now it's whomever bet the most/first if there's a tie
        expectedHighScore = MainConfig.default_chips

        self.refreshHandles()

        self.assertEqual(winner.highScore, expectedHighScore)

        # Close game
        self.assertIsNone(self.table.game)

    def refreshHandles(self):
        self.table.refresh_from_db()
        self.user1.refresh_from_db()
        self.user2.refresh_from_db()





