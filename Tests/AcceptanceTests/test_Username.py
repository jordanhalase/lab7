from django.test import Client
from django.test import TestCase

from Tests.TestingUtils import responseForCmd, bodyForCmd
from main import models
from main.messages import err_userNotFound


# PBI - As a user, I want to be able to have my name always displayed first in the command line
class TestUsername(TestCase):
    def setUp(self):
        self.client = Client()
        self.user = models.User.objects.create(admin=False, name='User')

    # Test post handler for rendered page error
    def test_UserNotExists(self):
        response = responseForCmd(self.client, "thisuserdoesnotexist", "refresh")
        self.assertNotEqual(response.find(err_userNotFound), -1, "couldn't find error string in body")


