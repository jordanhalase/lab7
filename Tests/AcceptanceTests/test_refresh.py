from django.test import TestCase
from main import models, messages
from main.messages import err_cmd_notAtTableRefresh
from main.apps import MainConfig
from main.cmd.factory import MakeCaller
from Tests.TestingUtils import assertStringContains


# PBI - User can refresh tables and games


class TestCreateTable(TestCase):
    def setUp(self):
        self.table1 = models.Table.objects.create(name="table1")
        self.user2 = models.User.objects.create(admin=False, name='user2')
        self.user = models.User.objects.create(admin=False, name='user')
        self.game = models.Game.objects.create()

    def test_refreshNotAtTable(self):
        resp = MakeCaller(self.user).execute('refresh')
        self.assertEqual(resp, err_cmd_notAtTableRefresh, "User can not refresh since they are at a table")

    def test_refreshAtTable(self):
        self.table1.addUser(self.user)

        resp = MakeCaller(self.user).execute('refresh')

        assertStringContains(resp, "Table: ")

        assertStringContains(resp, "Players: ")

    def test_refreshInGame(self):
        self.table1.addUser(self.user)
        self.table1.addUser(self.user2)

        u1 = MakeCaller(self.user)

        self.table1.refresh_from_db()
        self.user.refresh_from_db()
        self.user2.refresh_from_db()

        resp = u1.execute('refresh')
        assertStringContains(resp, "Pot: ")
        assertStringContains(resp, "Turn: ")
        assertStringContains(resp, "Chips: ")
        assertStringContains(resp, "Cards (")
