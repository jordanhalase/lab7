from django.test import TestCase
from main import models
from main.cmd.factory import MakeCaller


# PBI - tests to make sure admin has a special user name
from main.messages import err_cmd_createAdmin_nameValidate


class TestAdminName(TestCase):
    def setUp(self):
        self.admin = models.User.objects.create(admin=True, name='ADMIN')
        self.adminCaller = MakeCaller(self.admin)



    # test to make sure admin name is in all caps
    def test_nameGood(self):
        self.assertEqual(self.adminCaller.execute("create admin PARKER"), 'Admin "PARKER" created successfully')
        self.assertEqual(self.adminCaller.execute("create admin SPENCER"), 'Admin "SPENCER" created successfully')
        self.assertEqual(self.adminCaller.execute("create admin MYA"), 'Admin "MYA" created successfully')

    def test_nameBad(self):
        self.assertEqual(self.adminCaller.execute("create admin Admin"), err_cmd_createAdmin_nameValidate)
        self.assertEqual(self.adminCaller.execute("create admin admin"), err_cmd_createAdmin_nameValidate)
        self.assertEqual(self.adminCaller.execute("create admin steve"), err_cmd_createAdmin_nameValidate)
