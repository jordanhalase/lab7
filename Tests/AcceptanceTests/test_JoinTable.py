from django.test import TestCase
from main import models
from main.apps import MainConfig

from main.cmd.factory import MakeCaller


# PBI - User: I want to be able to join a table and have a game start if there's at least one other player
class TestJoinTable(TestCase):
    def setUp(self):
        self.table = models.Table.objects.create(name="table")
        self.user1 = models.User.objects.create(admin=False, name='user1')
        self.table.addUser(self.user1)

        self.user2 = models.User.objects.create(admin=False, name='user2')

# TODO these need to be join table not games
# testing to make sure player can not join game if already in a game
    def test_joinTableWhileAlreadyInTable(self):
        self.assertRaises(Exception, MakeCaller(self.user2).execute("join" + self.table.name))


# testing to make sure they can not join full or empty games
    def test_noTableAvailable(self):
        self.table.seats = 7
        self.assertRaises(Exception, MakeCaller(self.user1).execute("join" + self.table.name))



# testing to make sure player has 100 chips
    def test_playerChips(self):
        MakeCaller(self.user2).execute("join" + self.table.name)
        self.assertEqual(self.user1.chips, MainConfig.default_chips, "user should have the default number of chips")
        self.assertEqual(self.user2.chips, MainConfig.default_chips, "user should have the default number of chips")
