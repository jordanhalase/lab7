from django.test import TestCase
from main import models
from main.cmd.factory import MakeCaller

# PBI - testing to make sure admin can create users
from main.models import User


class AdminCreateUser(TestCase):
    def setUp(self):
        self.admin = models.User.objects.create(admin=True, name='ADMIN_STEVE')
        self.adminCaller = MakeCaller(self.admin)

        self.user = models.User.objects.create(admin=False, name='user')
        self.userCaller = MakeCaller(self.user)

    # test to make sure admin can be created by only an admin
    def test_createAdmin(self):
        self.assertEqual(self.adminCaller.execute('create admin ADMIN1'), 'Admin "ADMIN1" created successfully',
                         "wrong response")
        self.assertRaises(Exception, self.userCaller.execute('create admin ADMIN2'))


    # test that user is actually created
    def test_createUser(self):
        #testing method
        user = self.adminCaller.createUser("testuser1")
        users = list(User.objects.filter(name="testuser1"))
        self.assertTrue(len(users) > 0)
        self.assertEqual(users[0].name, "testuser1")

        #testing command call
        response = self.adminCaller.execute("create user testuser2")
        self.assertTrue(response.count("testuser2") != 0, "testuser2")

    # tests to make sure a user with an existing username cannot be created
    def test_createUserAlreadyExists(self):
        self.adminCaller.createUser("testuser3")
        self.assertRaises(Exception, self.adminCaller.createUser("testuser3"))
