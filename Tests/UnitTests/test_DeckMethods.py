from unittest import TestCase

from main.Deck import Deck


class Methods(TestCase):
    def setUp(self):
        self.deck = Deck()

    # Checks ValueError for the 53rd draw
    def test_draw(self):
        with self.assertRaises(ValueError) as ctx:
            numdraws = 53
            ctx.currentdraw = 0
            while ctx.currentdraw < numdraws:
                self.deck.draw()
                ctx.currentdraw += 1

        # Assert that the exception was raised on the 53rd draw and not earlier
        self.assertEqual(ctx.currentdraw + 1, 53)

    # Count of deck after drawing one card should be 51
    def test_count_one_draw(self):
        my_deck = Deck()
        my_deck.draw()
        self.assertEqual(my_deck.count(), 51)

    # Count of deck after drawing half the cards should be 26
    def test_count_26_draw(self):
        for x in range(26):
            self.deck.draw()
        self.assertEqual(self.deck.count(), 26)

    # Count of deck after drawing all 52 cards should be 0
    def test_count_draw_all(self):
        for x in range(52):
            self.deck.draw()
        self.assertEqual(self.deck.count(), 0)

    # Drawing with no cards left in deck should raise a ValueError
    def test_no_cards(self):
        for i in range(52):
            self.deck.draw()
        self.assertRaises(ValueError, self.deck.draw) #"should not be able to draw from an empty deck"

    # Once we have any implementation of the deck class we can get the cards,
    # for now we have to get them using the draw method. But since we can't insert cards,
    # there's no way to ensure a shuffled deck.
    def test_shuffle(self):
        shuffledDeck = Deck().shuffle()
        # Assert the order is not the same
        self.assertNotEqual(self.deck.deck, shuffledDeck.deck, "Cards were not shuffled")

