from unittest import TestCase
from unittest.mock import *

from main.Deck import Deck, MAX_LEN
from main.Card import Card


class Initialization(TestCase):
    def setUp(self):
        self.deck = Deck()

    # Assert that all cards in deck are of type Card
    def test_deck(self):
        for card in self.deck.deck:
            self.assertIsInstance(card, Card, "object in deck not an instance of Card")

    # Check that the deck starts with 52 cards
    def test_initialCount(self):
        self.assertEqual(self.deck.count(), 52, "invalid number of cards for a new deck")

    # Initial order is expected to be in numerical order of suit then rank
    # eg: [(1,1)...(1,13),(2,1)...(2,13)...]
    def test_initialOrder(self):
        for suit in range(1, 5):
            for rank in range(1, 14):
                expectedCard = Mock()  # Card(suit, rank)
                expectedCard.getSuit = MagicMock(return_value=suit)
                expectedCard.getRank = MagicMock(return_value=rank)
                actualCard = self.deck.draw()
                self.assertEqual(expectedCard.getSuit(), actualCard.getSuit(), "Default suit order not correct")
                self.assertEqual(expectedCard.getRank(), actualCard.getRank(), "Default rank order not correct")

    def test_serialization(self):
        deckStr = self.deck.toString()
        self.assertIsInstance(deckStr, str)
        self.assertEqual(len(deckStr), MAX_LEN())
        d1 = Deck.fromString(deckStr)
        self.assertEqual(len(self.deck.deck), len(d1.deck))
