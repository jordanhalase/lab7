from django.test import TestCase

from main.Deck import Deck
from main.models import Game, Table, User

class ModelGame(TestCase): #mya
    def setUp(self):
        self.game = Game.objects.create()
        self.table = Table.objects.create(name='table1')
        self.user1 = User.objects.create(name='user1')
        self.user2 = User.objects.create(name='user2')
        self.user3 = User.objects.create(name='user3')

    def test_canStart(self): #mya
        game = self.game
        table = self.table
        table.game = game
        table.save()
        self.user1.table = table
        self.user1.save()
        self.assertFalse(game.canStart(), "Game should not start, only one player")
        self.user2.table = table
        self.user2.save()
        self.assertTrue(game.canStart(),  "Game should be able to start, there are at least two players")

    def test_start(self): #mya

        self.table.game = self.game
        self.table.save()
        self.user1.table = self.table
        self.user1.save()

        self.user2.table = self.table
        self.user2.save()
        self.game.start()

        self.user3.table = self.table
        self.user3.save()

        self.user1.refresh_from_db()
        self.user3.refresh_from_db()
        self.assertEqual(self.user1.game, self.game, "Player should be in game")
        self.assertIsNone(self.user3.game, "User should not be in game")
        self.assertEqual(self.game.current_player, self.user1, "Should be first player's turn")

    def test_playerAction(self): #I don't understand -Mya
        pass

    def test_nextPlayer(self): #mya
        game = self.game
        self.user1.table = self.user2.table = self.table
        self.table.game = game
        self.user1.save()
        self.user2.save()
        game.start()
        self.assertEqual(game.current_player, self.user1, "Should be first player's turn")
        game.nextPlayer()
        self.assertEqual(game.current_player, self.user2, "Should be second player's turn")

    def test_getPlayer(self):
        #no players
        game = self.game
        self.assertListEqual(game.getPlayers(), [], "Should be no players")


        #two players
        self.user1.game = self.user2.game = self.game
        self.user1.save()
        self.user2.save()
        self.assertListEqual(game.getPlayers(), [self.user1, self.user2], "Should be two players")

        #three players
        self.user3.game = game
        self.user3.save()

        self.assertListEqual(game.getPlayers(), [self.user1, self.user2, self.user3], "Should be three players")

    def test_foldPlayer(self): #Mya
        self.user1.game = self.game

        self.assertRaises(Exception, self.user1.game.foldPlayer, "User not in game")

    def test_deck(self): #Ian
        game = self.game
        deck = game.deck
        deck2 = Deck()
        self.assertIsInstance(deck, Deck)

