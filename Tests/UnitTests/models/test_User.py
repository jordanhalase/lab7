from django.test import TestCase
from main.models import Game, Table, User


class ModelUser(TestCase):
    def setUp(self):
        self.game = Game.objects.create()
        self.table = Table.objects.create(name='userTable')
        self.user1 = User.objects.create(name='user1')
        self.user2 = User.objects.create(name='user2')

    # Returns true if user is at table, false otherwise
    def test_atTable(self):  # Sam
        self.user1.table = self.table
        self.user1.save()
        self.assertTrue(self.user1.atTable(), "user1 is at table userTable")
        self.assertFalse(self.user2.atTable(), "user2 is not at a table")

    # Returns true if user is in a game, false otherwise
    def test_inGame(self):  # Spencer
        self.user1.game = self.game
        self.user1.save()
        self.assertTrue(self.user1.inGame(), "user1 is in a game")
        self.assertFalse(self.user2.inGame(), "user2 is not in a game")

    # Returns true if user is waiting to join a game in-progress, false otherwise
    def test_inQueue(self):  # Sam
        self.user1.game_queue = Game.objects.create()
        self.user1.save()
        self.assertTrue(self.user1.inQueue(), "user1 is waiting in a queue")
        self.assertFalse(self.user2.inQueue(), "user2 is not waiting in a queue")

    # Updates the highscore of a user if their current chips are greater than it
    def test_highscore(self):  # Spencer
        self.assertEqual(self.user1.highScore, 0, "user1 does not have a highScore yet")
        self.user1.chips = 100
        self.user1.updateHighscore()
        self.user1.save()
        self.assertEqual(self.user1.highScore, 100, "user has a highsScore of 100")

    # Resets the user's chips to 0
    def test_resetChips(self):  # Sam
        self.user1.chips = 100
        self.user1.save()
        self.assertEqual(self.user1.chips, 100, "user1 has 100 chips")
        self.user1.resetChips()
        self.user1.save()
        self.assertEqual(self.user1.chips, 0, "user is broke")

    # Subtracts <amt> chips from the player,
    # returns true if successful, false otherwise
    # Precondition: amt is a positive integer
    def test_subChips(self):  # Spencer
        self.user1.chips = 100
        self.user1.save()
        self.assertEqual(self.user1.chips, 100, "user1 has 100 chips")
        self.assertTrue(self.user1.subChips(50), "user1 lost 50 chips")
        self.assertFalse(self.user1.subChips(100), "user cant lose more than what they have")

    # adds <amt> chips from the player,
    # Precondition: amt is a positive integer
    def test_addChips(self):  # Sam
        self.assertEqual(self.user1.chips, 100, "user1 is broke")
        self.user1.addChips(100)
        self.user1.save()
        self.assertEqual(self.user1.chips, 200, "user1 has 100 chips")

    # Gets called when the user joins a table
    def test_joinTable(self):  # Spencer
        self.user1.onJoinTable(self.table)
        self.assertEqual(self.user1.table, self.table, "user1 is at table userTable")

    # Gets called when a user leaves their table
    def test_leaveTable(self):  # Sam
        self.user1.table = self.table
        self.user1.onLeaveTable()
        self.assertEqual(self.user1.table, None, "user1 is no longer in a table")

    # Gets called when a user joins a game
    # User should receive the default amount of chips
    def test_joinGame(self):  # Spencer
        self.user1.onJoinGame(self.game)
        self.assertEqual(self.user1.game, self.game, "user1 is in game userGame")

    # Gets called when a user folds from a game
    # User should have their highscore updated and their chips reset
    def test_leaveGame(self):  # Sam
        self.user1.game = self.game
        self.user1.onLeaveGame()
        self.assertEqual(self.user1.game, None, "user1 is no longer in a table")
