from django.test import TestCase
from main.models import Table, User


class ModelTable(TestCase):
    def setUp(self): #Ian
        self.table = Table.objects.create(name='High Rollers')
        self.user1 = User.objects.create(name='user1')
        self.user2 = User.objects.create(name='user2')

    def test_getPlayers_empty(self): #Ian
        self.assertEqual(self.table.getPlayerCount(), 0, "table should have no players")
        self.assertListEqual(self.table.getPlayers(), [], "table should have no players")

    # returns a string of users at table
    def test_getPlayers_one(self): #Ian
        self.user1.table = self.table
        self.user1.save()
        self.assertEqual(self.table.getPlayerCount(), 1, "table should have one player")
        self.assertListEqual(self.table.getPlayers(), [self.user1], "table should have one player")

    #returns a string of users at table
    def test_getPlayers_two(self): #Ian
        self.user1.table = self.user2.table = self.table
        self.user1.save()
        self.user2.save()
        self.assertEqual(self.table.getPlayerCount(), 2, "table should have two players")
        self.assertListEqual(self.table.getPlayers(), [self.user1, self.user2], "table should have two players")

    #removes users from table when called
    def test_removePlayers(self): #mya
        table = self.table
        user1 = self.user1
        user2 = self.user2

        user1.table = user2.table = table
        user1.save()
        user2.save()

        table.removeUser(user2)
        table.save()
        self.assertEqual(table.getPlayerCount(), 1, "table should have one player")
        self.assertListEqual(table.getPlayers(), [user1], "table should have one player")

        table.removeUser(user1)
        table.save()
        self.assertEqual(table.getPlayerCount(), 0, "table should have no players")
        self.assertListEqual(table.getPlayers(), [], "table should have no players")

