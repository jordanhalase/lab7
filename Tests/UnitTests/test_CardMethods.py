from unittest import TestCase

from main.Card import Card


class Methods(TestCase):
    def setUp(self):
        # Four test cases
        self.fiveOfClubs = Card(1, 5)
        self.fiveOfSpades = Card(2, 5)
        self.aceOfHearts = Card(3, 1)
        self.threeOfDiamonds = Card(4, 3)

    def test_getSuit(self):
        msg = "getSuit() returning incorrect value"
        self.assertEqual(self.fiveOfClubs.getSuit(), 1, msg)
        self.assertEqual(self.fiveOfSpades.getSuit(), 2, msg)
        self.assertEqual(self.aceOfHearts.getSuit(), 3, msg)
        self.assertEqual(self.threeOfDiamonds.getSuit(), 4, msg)

    def test_getRank(self):
        msg = "getRank() returning incorrect value"
        for i in range(1, 13):
            c = Card(1, i)
            self.assertEqual(c.getRank(), i, msg)

    # Game-specific value for a card is assumed to be: suit * rank
    # eg: Ace of Hearts => suit: 3, rank: 1 => 3 * 1 = 3
    def test_getValue(self):
        # 3 * 1 = 3
        self.assertEqual(self.aceOfHearts.getValue(), 3)
        # 4 * 3 = 12
        self.assertEqual(self.threeOfDiamonds.getValue(), 12)
        # 1 * 5 = 5
        self.assertEqual(self.fiveOfClubs.getValue(), 5)
        # 2 * 5 = 10
        self.assertEqual(self.fiveOfSpades.getValue(), 10)

    # Tests the string value for:
    # each type of rank (J, Q, K, A, Number),
    # each suit (C, S, H, D)
    def test_stringValue(self):
        self.assertEqual(str(Card(1, 11)), 'JC')  # Jack of Clubs
        self.assertEqual(str(Card(4, 12)), 'QD')  # Queen of Diamonds
        self.assertEqual(str(Card(2, 13)), 'KS')  # King of Spades
        self.assertEqual(str(Card(2, 1)), 'AS')  # Ace of Spades
        self.assertEqual(str(Card(3, 3)), '3H')  # Three of Hearts
