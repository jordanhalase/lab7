from unittest import TestCase

from main.Card import Card

'''
Our run configuration collects all tests from Card and Deck test files in the tests directory.
No suites are needed to structure these tests. Run "UnitTests" to execute tests.
'''


class Initialization(TestCase):

    def test_bad_suit(self):
        # Negative Value
        self.assertRaises(ValueError, Card, (-1, 1), "Invalid Initial Value")

        # Zero Value
        self.assertRaises(ValueError, Card, (0, 1), "Invalid Initial Value")

        # Greater than range value
        self.assertRaises(ValueError, Card, (5, 1), "Invalid Initial Value")

    def test_bad_rank(self):
        # Negative Value
        self.assertRaises(ValueError, Card, (1, -1), "Invalid Initial Value")

        # Zero Value
        self.assertRaises(ValueError, Card, (1, 0), "Invalid Initial Value")

        # Greater than range value
        self.assertRaises(ValueError, Card, (1, 14), "Invalid Initial Value")

    def test_erroneous_input(self):
        # Erroneous string
        self.assertRaises(ValueError, Card, 'hello', 5)

        # Erroneous None-type
        self.assertRaises(ValueError, Card, 3, None)

        # Erroneous None-type
        self.assertRaises(ValueError, Card, None, 'hello')

        # Erroneous complex number
        self.assertRaises(ValueError, Card, 3 + 4j, 7)

        # Erroneous complex number
        self.assertRaises(ValueError, Card, 5, 6 + 8j)

        # Erroneous float
        self.assertRaises(ValueError, Card, 3.0, 4.0)

    def test_serialization(self):
        c1 = Card(1, 2)
        c1str = c1.toString()
        self.assertEqual(c1str, "12")
        c1 = Card.fromString(c1str)
        self.assertEqual(c1.getSuit(), 1)
        self.assertEqual(c1.getRank(), 2)
