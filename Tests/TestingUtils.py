
# Return the app's response string for a given username and command
import unittest


def responseForCmd(client, username, command):
    return rawResponseForCmd(client, username, command).context['response']

# Return the rendered app's response html
def bodyForCmd(client, username, command):
    response = rawResponseForCmd(client, username, command)
    return str(response.content)

# Return the django response object
def rawResponseForCmd(client, username, command):
    return client.post("", data={
        "command": command,
        "username": username
    })


def assertStringContains(string: str, value: str):
    contains = string.count(value) != 0
    if not contains:
        raise Exception(f'"{value}" not found in string')

